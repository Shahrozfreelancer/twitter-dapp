import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Login from './login';
import Twitter from './twitter';

function App() {
  return (
    <>
      <Router>
        
        <Routes>
            <Route exact path="/" element={<Login />} />
            <Route path="/twitter" element={<Twitter />} />
          
        </Routes>
        <Login></Login>
      </Router>
      
      </>    
  );
}

export default App;
