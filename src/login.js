import React from "react";
import { Link } from "react-router-dom";

function Login () {
    return(
        <>
             <div className="login-account">
                <Link to={"/twitter"} className="twitter-bg btn"><img src="images/twitter-white.png" alt=""></img> Login to your Twitter account</Link>
            </div>
        </>
    )
}

export default Login;